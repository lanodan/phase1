pkgname=gsed
pkgver=4.7
pkgrel=0
subpackages="$pkgname-doc"
pkgdesc="GNU stream editor"
url="https://www.gnu.org/software/sed"
arch="all"
license="GPL-3.0+"
makedepends="perl"
checkdepends="diffutils"
install="$pkgname.post-deinstall"
source="https://ftp.heanet.ie/mirrors/ftp.gnu.org/gnu/sed/sed-$pkgver.tar.xz
	disable-mbrtowc-test.patch
	"
# testsuite fails because busybox provides a /usr/bin/timeout
options="!checkroot !check"
builddir="$srcdir/sed-$pkgver"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-i18n \
		--disable-nls \
		--program-prefix=g
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias || true
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="e0be5db4cdf8226b34aaa9071bc5ae0eafde1c52227cee3512eea7fe2520d6c5cebf15266aa5c4adffbb51bf125c140a15644e28d57759893c12823ea9bbf4fb  sed-4.7.tar.xz
aeb55f85a5c724f0dacbf2f39e0f99ae4c66159115b00aa36d65f234f87e52e660878cb18b772a494349632dfa1b616b9306a4cafe87e91182ea8936c308506a  disable-mbrtowc-test.patch"
