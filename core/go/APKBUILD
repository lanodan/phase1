pkgname=go
pkgver=1.13.3
pkgrel=0
pkgdesc="Go programming language compiler"
url="https://golang.org/"
arch="all"
license="BSD-3-Clause"
makedepends="bash clang llvm-binutils go"
subpackages="$pkgname-doc"
source="https://golang.org/dl/go${pkgver/_/}.src.tar.gz
	default-buildmode-pie.patch
	set-external-linker.patch
	"
options="!check"

# The following tests fail currently, most of them fail due to linking
# errors likely related to 'default-buildmode-pie.patch' or musl libc.
_brokentests="runtime debug/gosym cmd/.* cgo.* go_test:os/exec ^test.* exec_linux_test"

case "$CTARGET_ARCH" in
aarch64)export GOARCH="arm64" ;;
x86_64) export GOARCH="amd64" ;;
ppc64le) export GOARCH="ppc64le" ;;
mips64) export GOARCH="mips64" GOMIPS64=softfloat ;;
*)      export GOARCH="unsupported";;
esac

builddir="$srcdir"/go

export CC_FOR_TARGET="$CC"
export CC="${HOSTLD:-clang}"
export CXX="${HOSTLD:-clang++}"
export LD="${HOSTLD:-ld}"

build() {
	cd "$builddir/src"

	export GOOS="linux"
	export GOPATH="$srcdir"
	export GOROOT="$builddir"
	export GOBIN="$GOROOT"/bin
	export GOROOT_FINAL=/usr/lib/go
	export GOROOT_BOOTSTRAP=/usr/lib/go

	./make.bash
}

check() {
	cd "$builddir/src"
	PATH="$builddir/bin:$PATH" ./run.bash -no-rebuild \
		-run "!($(echo "$_brokentests" | tr " " "|"))$"
}

package() {
	mkdir -p "$pkgdir"/usr/bin "$pkgdir"/usr/lib/go/bin "$pkgdir"/usr/share/doc/go

	for binary in go gofmt; do
		install -Dm755 bin/"$binary" "$pkgdir"/usr/lib/go/bin/"$binary"
		ln -s /usr/lib/go/bin/"$binary" "$pkgdir"/usr/bin/
	done

	cp -a pkg lib "$pkgdir"/usr/lib/go
	cp -r doc misc "$pkgdir"/usr/share/doc/go
	rm -rf "$pkgdir"/usr/lib/go/pkg/obj
	rm -rf "$pkgdir"/usr/lib/go/pkg/bootstrap
	rm -f  "$pkgdir"/usr/lib/go/pkg/tool/*/api

	mkdir -p "$pkgdir"/usr/lib/go/
	cp -a "$builddir"/src "$pkgdir"/usr/lib/go

	# Remove tests from /usr/lib/go/src.
	# Those shouldn't be affacted by the upstream bug (see above).
	find "$pkgdir"/usr/lib/go/src \( -type f -a -name "*_test.go" \) \
		-exec rm -rf \{\} \+
	find "$pkgdir"/usr/lib/go/src \( -type d -a -name "testdata" \) \
		-exec rm -rf \{\} \+
	find "$pkgdir"/usr/lib/go/src -type f -a \( -name "*.bash" -o -name "*.rc" -o -name "*.bat" \) \
		-exec rm -rf \{\} \+
}

sha512sums="0999876f995a3d9189640ce15b496ab72a6273649d27acdc190c1d50b88ab8b7facaabfc832334911d178f0b9a645ea4169716ed5c593a7540b075e6901d51f2  go1.13.3.src.tar.gz
f0c07d9979fc3165fc78158406de8440624b3f2c6f6542c9889c71efbf3d2f02a7ffee27ccba8c2630489895d331b7b9d3a606162134dcb3e8e0b9fc06b529dc  default-buildmode-pie.patch
faf8de430df185842902322f064254f3e9ecee0884b3075b5550c85da15ff61ea6c2bb8d0fb7cf3887abc0e40974bd73ee8f8c14da7f914dde7e9220177c4e2a  set-external-linker.patch"
