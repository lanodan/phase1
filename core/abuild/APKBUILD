pkgname=abuild
pkgver=4.0.9
pkgrel=0
pkgdesc="Script to build Alpine Packages"
url="https://git.alpinelinux.org/cgit/abuild/"
arch="all"
license="GPL-2.0"
depends="fakeroot sudo pax-utils openssl apk-tools>=2.0.7-r1 bsdtar
	 attr pkgconf patch bzip2 xz gzip zlib lzip diffutils flex 
	 bison gtar unzip gmake file"
if [ "$CBUILD" = "$CHOST" ]; then
	depends="$depends curl"
fi
makedepends_build="pkgconfig"
makedepends_host="openssl-dev zlib-dev"
makedepends="$makedepends_host $makedepends_build"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="apkbuild-cpan:cpan:noarch apkbuild-gem-resolver:gems:noarch
             abuild-rootbld:_rootbld:noarch $pkgname-doc"
options="suid !check"
pkggroups="abuild"
source="https://gitlab.com/abyssos/abuild/-/archive/v$pkgver/abuild-v$pkgver.tar.gz"

builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare

	cd "$builddir"
	sed -i -e "/^CHOST=/s/=.*/=$CHOST/" abuild.conf
}

build() {
	cd "$builddir"
	make VERSION="$pkgver-r$pkgrel"
}

package() {
	cd "$builddir"

	make install VERSION="$pkgver-r$pkgrel" DESTDIR="$pkgdir"

	install -m 644 abuild.conf "$pkgdir"/etc/abuild.conf
	install -d -m 775 -g abuild "$pkgdir"/var/cache/distfiles
}

cpan() {
	pkgdesc="Script to generate perl APKBUILD from CPAN"
	depends="perl perl-libwww perl-json perl-module-build-tiny perl-lwp-protocol-https"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/apkbuild-cpan "$subpkgdir"/usr/bin/
}

gems() {
	pkgdesc="APKBUILD dependency resolver for RubyGems"
	depends="ruby ruby-augeas"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/apkbuild-gem-resolver "$subpkgdir"/usr/bin/
}

_rootbld() {
	pkgdesc="Build packages in chroot"
	depends="abuild bubblewrap gettext git"
	mkdir -p "$subpkgdir"
}

sha512sums="38712c6a5bb9a3eaa2e688724751ce1bf301ecedb4bc779069d4bf1ea48de8fd2f7a095c9266ef38fad6c40af8d84f27f2de8a2a2cde853db8878bc30e2213e8  abuild-v4.0.9.tar.gz"
