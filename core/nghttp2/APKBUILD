pkgname=nghttp2
pkgver=1.39.1
pkgrel=0
pkgdesc="Experimental HTTP/2 client, server and proxy"
url="https://nghttp2.org"
arch="all"
license="MIT"
makedepends_build="autoconf automake libtool"
makedepends_host="libev-dev openssl-dev zlib-dev c-ares-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="https://github.com/tatsuhiro-t/$pkgname/releases/download/v$pkgver/nghttp2-$pkgver.tar.xz
remove-mruby-tests.patch"
builddir="$srcdir"/$pkgname-$pkgver

check() {
	cd "$builddir"
	make check
	# integration tests require go, which is only available
	# in community at the moment of writing. Disabling until
	# go is moved into main
#	cd "integration-tests"
#	make itprep
#	make it
}

build() {
	cd "$builddir"
	autoreconf -vif
	CC=clang CXX=clang++ ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/usr/bin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--without-libxml2 \
		--without-neverbleed \
		--without-jemalloc \
		--disable-python-bindings \
		--enable-static
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="36558ed03c59086086abbf144ec7c54c60de3fea00a9ea594feea7186a779781cbb66a08c9b34265892dea382b42b875f551e85331cfa0086e357f9b27b919fa  nghttp2-1.39.1.tar.xz
d3f6a66ad6522babb5ad2b3721d52c1c2af88e57ed2895cf87037da1032ca42dcb95dacc23ea277b9507b4116cec117b5c9a3313759dc56b48199b687b74dd9a  remove-mruby-tests.patch"
