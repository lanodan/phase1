pkgname=musl
pkgver=1.1.24
pkgrel=0
pkgdesc="the musl c library (libc) implementation"
url="http://www.musl-libc.org/"
arch="all"
license="MIT"
depends="filesystem"
subpackages="$pkgname-dev libc6-compat:compat:noarch $pkgname-utils"
makedepends="musl-dev filesystem"
source="http://www.musl-libc.org/releases/musl-$pkgver.tar.gz
	__stack_chk_fail_local.c
	handle-aux-at_base.patch
	ldconfig
	getconf.c
	getent.c
	iconv.c
	strndupa.patch
	sys-cdefs.h
	sys-queue.h
	sys-tree.h
	"
provides="bsd-compat-headers"
replaces="libc-dev"
builddir="$srcdir"/musl-$pkgver
_cflags=""

build() {
	cd "$builddir"

	[ "$BOOTSTRAP" = "nocc" ] && return 0

	if [ "$CC" = "gcc" ]; then
		# provide minimal libssp_nonshared.a so we don't need libssp from gcc
		$CC $CPPFLAGS $CFLAGS -c "$srcdir"/__stack_chk_fail_local.c -o __stack_chk_fail_local.o
		ar r libssp_nonshared.a __stack_chk_fail_local.o
	fi

	# getconf/getent/iconv
	local i
	for i in getconf getent iconv ; do
		$CC $CPPFLAGS $CFLAGS "$srcdir"/$i.c -o $i
	done

	# note: not autotools
	LDFLAGS="$LDFLAGS -Wl,-soname,libc.musl-${CARCH}.so.1" \
	CC=$CC CXX=$CXX ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--libdir=/usr/lib \
		--syslibdir=/usr/lib \
		--bindir=/usr/bin \
		--sbindir=/usr/bin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make -j${JOBS}
}

package() {
	cd "$builddir"

	case "$CARCH" in
	aarch64*)	ARCH="aarch64" ;;
	x86_64)		ARCH="x86_64" ;;
	ppc64*)		ARCH="powerpc64" ;;
	mips64*)	ARCH="mips64" ;;
	esac

	if [ "$BOOTSTRAP" = "nocc" ]; then
		make ARCH="$ARCH" prefix=/usr DESTDIR="$pkgdir" install-headers
	else
		make DESTDIR="$pkgdir" install

		if [ "$CC" = "gcc" ]; then
			cp libssp_nonshared.a "$pkgdir"/usr/lib
		fi

		# make LDSO the be the real file, and libc the symlink
		local LDSO=$(make -f Makefile --eval "$(echo -e 'print-ldso:\n\t@echo $$(basename $(LDSO_PATHNAME))')" print-ldso)
		mv -f "$pkgdir"/usr/lib/libc.so "$pkgdir"/usr/lib/"$LDSO"
		ln -svf "$LDSO" "$pkgdir"/usr/lib/libc.musl-${CARCH}.so.1
		ln -svf "$LDSO" "$pkgdir"/usr/lib/libc.so

		mkdir -p "$pkgdir"/usr/bin
		ln -sv /usr/lib/"$LDSO" $pkgdir/usr/bin/ldd

		rm -v $pkgdir/usr/include/libintl.h
	fi

	install -m644 $srcdir/sys-cdefs.h $pkgdir/usr/include/sys/cdefs.h
	install -m644 $srcdir/sys-queue.h $pkgdir/usr/include/sys/queue.h
	install -m644 $srcdir/sys-tree.h $pkgdir/usr/include/sys/tree.h

}

utils() {
	depends="scanelf"
	replaces="libiconv"
	license="MIT BSD GPL2+"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/usr/sbin
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/

	install -D \
		"$builddir"/getent \
		"$builddir"/getconf \
		"$builddir"/iconv \
		"$subpkgdir"/usr/bin

	install -D -m755 "$srcdir"/ldconfig "$subpkgdir"/usr/sbin
}

compat() {
	pkgdesc="compatibility libraries for glibc"

	mkdir -p "$subpkgdir"/usr/lib
	# definitive source is https://sourceware.org/glibc/wiki/ABIList
	case "$CARCH" in
	aarch64)	_ld="usr/lib/ld-linux-aarch64.so.1" ;;
	mips64)		_ld="usr/lib64/ld.so.1" ;;
	ppc64le)	_ld="usr/lib64/ld64.so.2" ;;
	x86_64)		_ld="usr/lib64/ld-linux-x86-64.so.2";;
	esac
	mkdir -p "$subpkgdir/${_ld%/*}"
	ln -sf "/usr/lib/libc.musl-${CARCH}.so.1" "$subpkgdir/$_ld"

	for i in libc.so.6 libcrypt.so.1 libm.so.6 libpthread.so.0 librt.so.1 libutil.so.1; do
		ln -sf "/usr/lib/libc.so" "$subpkgdir/usr/lib/$i"
	done
}

sha512sums="8987f1e194ea616f34f4f21fe9def28fb7f81d7060e38619206c6349f79db3bbb76bae8b711f5f9b8ed038799c9aea1a4cbec69e0bc4131e246203e133149e77  musl-1.1.24.tar.gz
062bb49fa54839010acd4af113e20f7263dde1c8a2ca359b5fb2661ef9ed9d84a0f7c3bc10c25dcfa10bb3c5a4874588dff636ac43d5dbb3d748d75400756d0b  __stack_chk_fail_local.c
6a7ff16d95b5d1be77e0a0fbb245491817db192176496a57b22ab037637d97a185ea0b0d19da687da66c2a2f5578e4343d230f399d49fe377d8f008410974238  handle-aux-at_base.patch
8d3a2d5315fc56fee7da9abb8b89bb38c6046c33d154c10d168fb35bfde6b0cf9f13042a3bceee34daf091bc409d699223735dcf19f382eeee1f6be34154f26f  ldconfig
0d80f37b34a35e3d14b012257c50862dfeb9d2c81139ea2dfa101d981d093b009b9fa450ba27a708ac59377a48626971dfc58e20a3799084a65777a0c32cbc7d  getconf.c
378d70e65bcc65bb4e1415354cecfa54b0c1146dfb24474b69e418cdbf7ad730472cd09f6f103e1c99ba6c324c9560bccdf287f5889bbc3ef0bdf0e08da47413  getent.c
9d42d66fb1facce2b85dad919be5be819ee290bd26ca2db00982b2f8e055a0196290a008711cbe2b18ec9eee8d2270e3b3a4692c5a1b807013baa5c2b70a2bbf  iconv.c
fffce654891d354767f2b58cf1fe74911995e270e5d861fbebd20304612280f4f6e0bd07fea4c72a7fa01021f536b4e491cefc97792f710acaacb6b73fc38313  strndupa.patch
8c3fddd73b696a38e633953715c79c47703739be27ee085fc6c960a57b6746ca05bf6406f7e6126cc1a13204254fd5830afb566624e1f298f4d6b58216013c28  sys-cdefs.h
2f0d5e6e4dc3350285cf17009265dddcbe12431c111868eea39bc8cb038ab7c1f2acacbb21735c4e9d4a1fd106a8fc0f8611ea33987d4faba37dde5ce6da0750  sys-queue.h
07cb70f2f0ddb31e23dd913c6f561fc9885667c5803fdf3a559676c99d08834b4104589bacb5d17b4a0b379c68c81a1cf3173832b3da33a7b936fa7b93706844  sys-tree.h"
